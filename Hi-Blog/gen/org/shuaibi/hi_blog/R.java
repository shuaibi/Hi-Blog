/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package org.shuaibi.hi_blog;

public final class R {
    public static final class attr {
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int rightPadding=0x7f010000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int abc_ic_ab_back_mtrl_am_alpha=0x7f020000;
        public static final int abc_ic_menu_share_mtrl_alpha=0x7f020001;
        public static final int abc_list_divider_mtrl_alpha=0x7f020002;
        public static final int ic_comment_white_18dp=0x7f020003;
        public static final int ic_launcher=0x7f020004;
        public static final int ic_release=0x7f020005;
        public static final int ic_search_white_36dp=0x7f020006;
        public static final int ic_stars_white_18dp=0x7f020007;
        public static final int ic_visibility_white_18dp=0x7f020008;
    }
    public static final class id {
        public static final int blogdetail_iv_comment=0x7f070002;
        public static final int comment_llv_commentList=0x7f070006;
        public static final int comment_tv_content=0x7f070014;
        public static final int comment_tv_number=0x7f070011;
        public static final int comment_tv_sendTime=0x7f070013;
        public static final int comment_tv_userName=0x7f070012;
        public static final int id_menu=0x7f070008;
        public static final int iv_back=0x7f070007;
        public static final int iv_bolgdetail_back=0x7f070000;
        public static final int main_civ_user_head=0x7f070015;
        public static final int main_lv_bolg_list=0x7f07000a;
        public static final int main_lv_bolg_logo=0x7f070009;
        public static final int tv_blog_comment=0x7f07000f;
        public static final int tv_blog_content=0x7f07000e;
        public static final int tv_blog_createTime=0x7f07000c;
        public static final int tv_blog_title=0x7f07000d;
        public static final int tv_blog_userName=0x7f07000b;
        public static final int tv_blog_visibility=0x7f070010;
        public static final int tv_blogdetail_comfrom=0x7f070004;
        public static final int tv_blogdetail_content=0x7f070005;
        public static final int tv_blogdetail_ptitle=0x7f070003;
        public static final int tv_blogdetail_title=0x7f070001;
    }
    public static final class layout {
        public static final int activity_blogdetail=0x7f030000;
        public static final int activity_comment=0x7f030001;
        public static final int activity_init=0x7f030002;
        public static final int activity_login=0x7f030003;
        public static final int activity_main=0x7f030004;
        public static final int footer_layout=0x7f030005;
        public static final int item_blog=0x7f030006;
        public static final int item_comment=0x7f030007;
        public static final int left_menu=0x7f030008;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int login=0x7f050001;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
    public static final class styleable {
        /** Attributes that can be used with a SlidingMenu.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #SlidingMenu_rightPadding org.shuaibi.hi_blog:rightPadding}</code></td><td></td></tr>
           </table>
           @see #SlidingMenu_rightPadding
         */
        public static final int[] SlidingMenu = {
            0x7f010000
        };
        /**
          <p>This symbol is the offset where the {@link org.shuaibi.hi_blog.R.attr#rightPadding}
          attribute's value can be found in the {@link #SlidingMenu} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name org.shuaibi.hi_blog:rightPadding
        */
        public static final int SlidingMenu_rightPadding = 0;
    };
}
