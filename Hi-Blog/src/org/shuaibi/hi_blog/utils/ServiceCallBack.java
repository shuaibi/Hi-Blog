package org.shuaibi.hi_blog.utils;

import org.json.JSONObject;

/**
 * 
 * @描述: 服务器返回值回调
 * @作者: ZeroX
 * @时间: 2015-9-16
 * @参数： 
 * @接口： 
 *
 */
public interface ServiceCallBack {
	void serviceResult(JSONObject jsonObject,int mark);
}
