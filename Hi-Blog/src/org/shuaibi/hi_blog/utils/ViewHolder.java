package org.shuaibi.hi_blog.utils;





import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 
 * @描述: ViewHolder
 * @作者: ZeroX
 * @时间: 2015-9-17
 * @参数： 
 * @接口： 
 *
 */
public class ViewHolder {

	private SparseArray<View> mView;
	private View mConvertView;
	public int mPosition;


	public ViewHolder(Context context,ViewGroup parent,int position,int layoutId)
	{
		mView=new SparseArray<View>();
		mPosition=position;
		mConvertView=LayoutInflater.from(context).inflate(layoutId, parent,false);
		mConvertView.setTag(this);
	}

	public	ViewHolder ()
	{

	}
	public static ViewHolder get(Context context,View convertView,ViewGroup parent,int position,int layoutId)
	{
		if(convertView==null)
		{
			return new ViewHolder(context, parent, position, layoutId);
		}else {
			ViewHolder viewHolder=(ViewHolder) convertView.getTag();
			viewHolder.mPosition=position;
			return viewHolder;
		}

	}

	@SuppressWarnings("unchecked")
	public<T extends View> T getView(int viewId)
	{
		View view=mView.get(viewId);
		if(view==null)
		{
			view=mConvertView.findViewById(viewId);
			mView.put(viewId, view);
		}
		return (T) view;

	}

	public View getConvertView()
	{
		return mConvertView;
	}
	
	
	
}
