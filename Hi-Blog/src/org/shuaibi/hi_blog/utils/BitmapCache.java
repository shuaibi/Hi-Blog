package org.shuaibi.hi_blog.utils;

import com.android.volley.toolbox.ImageLoader.ImageCache;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * 
 * @描述: 图片缓存
 * @作者: ZeroX
 * @时间: 2015-9-17
 * @参数： 
 * @接口： 
 *
 */
public class BitmapCache implements ImageCache {

	private LruCache<String, Bitmap> mCache;

	public BitmapCache() {

		int maxSize = 4 * 1024 * 1024;

		mCache = new LruCache<String, Bitmap>(maxSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {

				return bitmap.getRowBytes() * bitmap.getHeight();
			}

		};

	}

	@Override
	public Bitmap getBitmap(String url) {
		return mCache.get(url);
	}

	@Override
	public void putBitmap(String url, Bitmap bitmap) {
		if (bitmap != null) {

			mCache.put(url, bitmap);

		}

	}
}
