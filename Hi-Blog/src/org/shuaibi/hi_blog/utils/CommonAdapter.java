package org.shuaibi.hi_blog.utils;


import java.util.List;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;




import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * 
 * @描述: 公共适配器
 * @作者: ZeroX
 * @时间: 2015-9-17
 * @参数： 
 * @接口： 
 *
 */
public abstract class CommonAdapter<T> extends BaseAdapter {
	
	protected List<T> mList;
	protected Context mContext;
	protected RequestQueue mQueue;
	protected ImageLoader mImageLoader;
	
	public CommonAdapter(){
		
	}
	public CommonAdapter(List<T> list,Context context)
	{
		this.mList=list;
		this.mContext=context;
		this.mQueue = Volley.newRequestQueue(context);
	    this.mImageLoader = new ImageLoader(mQueue, new BitmapCache());
	}

	@Override
	public int getCount() {
		
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return position;
	}

	@Override
	public abstract View getView(int position, View convertView, ViewGroup parent);
		
	
	

}
