package org.shuaibi.hi_blog.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.shuaibi.hi_blog.R;
import org.shuaibi.hi_blog.adapter.BlogAdapter;
import org.shuaibi.hi_blog.utils.CallWebservice;
import org.shuaibi.hi_blog.utils.Config;
import org.shuaibi.hi_blog.utils.ServiceCallBack;
import org.shuaibi.hi_blog.view.CircleImageView;
import org.shuaibi.hi_blog.view.LoadListView;
import org.shuaibi.hi_blog.view.SlidingMenu;
import org.shuaibi.hi_blog.view.LoadListView.ILoadInterface;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.app.Activity;
import android.content.Intent;

/**
 * 
 * @描述: 
 * @作者: ZeroX
 * @时间: 2015-9-15
 * @参数： 
 * @接口： 
 *
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends Activity implements ServiceCallBack,ILoadInterface{

	public final int GETBLOGLIST=1; //获取博客列表
	public int mIndex=1,mPageSize=10,mPageCount;

	@ViewById(R.id.main_lv_bolg_list)
	LoadListView mBloListView; //博客列表View

	@ViewById(R.id.main_lv_bolg_logo)
	ImageView mBlogLogo;

	@ViewById(R.id.id_menu)
	SlidingMenu mSlidingMenu;

	@ViewById(R.id.main_civ_user_head)
	CircleImageView mUserHead;
	List<HashMap<String, String>> mBlogList;//博客列表Data

	BlogAdapter mBlogAdapter; //博客适配器

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBlogList=new ArrayList<HashMap<String,String>>();
		mBlogAdapter=new BlogAdapter(this,mBlogList);

	}

	@AfterViews
	public void init()
	{
		loadBlogData();
		mBloListView.setInterface(this);
		mBloListView.setAdapter(mBlogAdapter);
		mBloListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view , int position,
					long arg3) {
				@SuppressWarnings("unchecked")
				HashMap<String, String> itemHashMap=(HashMap<String, String>) parent.getItemAtPosition(position);
				Intent intent=new Intent(MainActivity.this,BlogDetailActivity_.class);
				intent.putExtra("BlogId", itemHashMap.get("ID"));
				intent.putExtra("UserName", itemHashMap.get("UserName"));
				startActivity(intent);
			}
		});

	}

	@Override
	public void serviceResult(JSONObject jsonObject, int mark) {
		try {
			switch (mark) {
			case GETBLOGLIST:
				blogResultHander(jsonObject.getJSONArray("blog"));
				//mPageCount=(jsonObject.getInt("total")+mPageSize-1)/mPageSize;
				mPageCount=jsonObject.getInt("total");
				break;
			default:
				break;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Click(R.id.main_lv_bolg_logo)
	public void blogLogo_click()
	{
		mSlidingMenu.toggle();
	}
	@Click(R.id.main_civ_user_head)
	public void userHead_Click()
	{
		Intent intent =new Intent();
          if(Config.ISLOGIN)  //打开用户详细
          {
        	  
          }else  //打开登录
          {
        	  intent.setClass(this,LoginActivity_.class);
          }
          startActivity(intent);
	}
	/**
	 * 博客列表返回值处理
	 * @param jsonArray 博客列表
	 */
	public void blogResultHander(JSONArray jsonArray) throws Exception
	{
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject=jsonArray.getJSONObject(i);
			HashMap<String, String> itemHashMap=new HashMap<String, String>();
			itemHashMap.put("ID", jsonObject.getString("Id"));
			itemHashMap.put("UserName", jsonObject.getJSONObject("BlogUsersSet").getString("UserNickname"));
			itemHashMap.put("CreateTime", jsonObject.getString("BlogCreateTime"));
			itemHashMap.put("Title", jsonObject.getString("BlogTitle"));
			itemHashMap.put("Content", jsonObject.getString("BlogContent"));
			itemHashMap.put("Comment", jsonObject.getString("BlogCommentNum"));
			itemHashMap.put("Visibility", jsonObject.getString("BlogReadNum"));
			mBlogList.add(itemHashMap);
		}

		mBlogAdapter.notifyDataSetChanged();
		mBloListView.loadCompany();

	}

	@Override
	public void loadMoreData() {
		if(mPageCount>=mIndex)
		{
			mIndex++;
			loadBlogData();
			if(mPageCount==mIndex)
			{
				mBloListView.setLoading(false);
			};
		}
	}

	/**
	 * 加载博客列表
	 */
	public void loadBlogData()
	{
		HashMap<String, String> params=new HashMap<String, String>();
		params.put("idex",mIndex+"");
		params.put("sizePage", mPageSize+"");
		params.put("ContentLength", "80");
		new CallWebservice(this,"get",params,GETBLOGLIST);



	}
}
