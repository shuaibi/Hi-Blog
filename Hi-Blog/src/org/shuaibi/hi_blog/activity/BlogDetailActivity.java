package org.shuaibi.hi_blog.activity;

import java.util.HashMap;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;
import org.shuaibi.hi_blog.R;
import org.shuaibi.hi_blog.utils.CallWebservice;
import org.shuaibi.hi_blog.utils.ServiceCallBack;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 
 * @描述: 博客详细
 * @作者: ZeroX
 * @时间: 2015-9-19
 * @参数： 
 * @接口： 
 *
 */
@SuppressLint("SetJavaScriptEnabled")
@EActivity(R.layout.activity_blogdetail)
public class BlogDetailActivity extends Activity implements ServiceCallBack {

	@ViewById(R.id.tv_blogdetail_title)
	TextView mTitle; //界面标题
	@ViewById(R.id.tv_blogdetail_ptitle)
	TextView mPTitle; //博客标题
	@ViewById(R.id.tv_blogdetail_comfrom)
	TextView mComfrom; //来自
	@ViewById(R.id.tv_blogdetail_content)
	WebView mContent; //内容
	@ViewById(R.id.iv_bolgdetail_back)
	ImageView mBack; //返回按钮
	@ViewById(R.id.blogdetail_iv_comment)
	ImageView mComment; //评论按钮
	
	String mBlogId,//博客ID
	       mUserName;//用户名
	final int  GETBLOGDETAIL=1; //获取博客详细
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBlogId=getIntent().getStringExtra("BlogId");
		mUserName=getIntent().getStringExtra("UserName");
	}
	
	@AfterViews
	public void initView()
	{
		mContent.setHorizontalScrollBarEnabled(false);
		mContent.setBackgroundColor(Color.parseColor("#2D2D2D"));
		mContent.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View arg0) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		//接口参数
		HashMap<String, String> params=new HashMap<String, String>();
		params.put("blogId", mBlogId);
		//获取博客详细
		new CallWebservice(this, "GetBlogInfo", params, GETBLOGDETAIL);
	}
	
	/**
	 * 返回点击事件
	 */
	@Click(R.id.iv_bolgdetail_back)
	public void back_Click()
	{
		finish();
	}

	/**
	 * 评论点击事件  
	 */
	@Click(R.id.blogdetail_iv_comment)
	public void comment_Click()
	{
		Intent intent=new Intent(BlogDetailActivity.this, CommentActivity_.class);
		intent.putExtra("BlogId", mBlogId);
		startActivity(intent);
	}
	
	
	@Override
	public void serviceResult(JSONObject jsonObject, int mark) {
		
		try {
			switch (mark) {
			case GETBLOGDETAIL:
				blogDetailHander(jsonObject.getJSONObject("Blog"));
				break;

			default:
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void blogDetailHander(JSONObject jsonObject) throws Exception
	{
		mTitle.setText(jsonObject.getString("BlogTitle"));
		mPTitle.setText(jsonObject.getString("BlogTitle"));
		mComfrom.setText(mUserName+"  "+jsonObject.getString("BlogCreateTime"));
	    mContent.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
		mContent.getSettings().setJavaScriptEnabled(true);
		mContent.loadData(getHtmlData(jsonObject.getString("BlogContent")),"text/html; charset=utf-8", "utf-8");
	
		
	}
	
	private String getHtmlData(String bodyHTML) {
	    String head = "<head>" +
	                "<meta name=\"viewport\"   content=\"width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no\" />" +
	                "<style>img{max-width: 100%; width:auto; height:auto;} *{color:#BEBEBE}</style>" +
	                "<link type=\"text/css\" rel=\"stylesheet\" href=\"http://www.cnblogs.com/bundles/blog-common.css?v=VpfXXGDfdkCz7-E0_XQ205E77kSNAF0FnyWpIgmGioM1\"/>"+
	                "<script src=\"http://common.cnblogs.com/script/jquery.js\" type=\"text/javascript\"></script>"+
	                "<script src=\"http://common.cnblogs.com/bundles/blog-common.js?v=KJbWl4pDSR547B6rgxi3B-dDHKVpKi8xHNL9Qhc1rkk1\" type=\"text/javascript\"></script>"+
	                "</head>";
	    return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
	}
}
