package org.shuaibi.hi_blog.activity;

import org.androidannotations.annotations.EActivity;
import org.shuaibi.hi_blog.R;

import android.app.Activity;
import android.os.Bundle;

@EActivity(R.layout.activity_init)
public class InitActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
}
