package org.shuaibi.hi_blog.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.shuaibi.hi_blog.R;
import org.shuaibi.hi_blog.adapter.CommentAdapter;
import org.shuaibi.hi_blog.utils.CallWebservice;
import org.shuaibi.hi_blog.utils.ServiceCallBack;
import org.shuaibi.hi_blog.view.LoadListView;
import org.shuaibi.hi_blog.view.LoadListView.ILoadInterface;

import android.app.Activity;
import android.os.Bundle;

/**
 * 
 * @ClassName: CommentActivity 
 * @Description: 博客评论 
 * @author ZeroX
 * @date 2015-9-29
 *
 */
@EActivity(R.layout.activity_comment)
public class CommentActivity extends Activity implements ServiceCallBack,ILoadInterface {

	String  mBlogId; //博客ID
	int mIndex=1,mPageSize=10,mPageCount;
	final int GETCOMMENTLIST=1; // 获取博客列表

	List<HashMap<String,String>> mCommentList;
	CommentAdapter  mCommentAdapter;
	
	@ViewById(R.id.comment_llv_commentList)
	LoadListView mCommentListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//获取上个Activity传过来的值
		mBlogId=getIntent().getExtras().getString("BlogId");
		mCommentList=new ArrayList<HashMap<String,String>>();
		mCommentAdapter=new CommentAdapter(this, mCommentList);

	}
	
	@AfterViews
	public void init()
	{
		loadCommentData();
		mCommentListView.setInterface(this);
		mCommentListView.setAdapter(mCommentAdapter);
	}

	@Override
	public void serviceResult(JSONObject jsonObject, int mark) {

		try {
			switch (mark) {
			case GETCOMMENTLIST:
				mPageCount=jsonObject.getInt("Total");
				commentHandler(jsonObject.getJSONArray("CommentList"));
				break;

			default:
				break;
			}
		} catch (JSONException e) {

			e.printStackTrace();
		} 

	}

	/**
	 * 评论列表处理
	 * @param jsonArray 评论列表
	 * @throws JSONException JSON转换异常
	 */
	public void commentHandler(JSONArray jsonArray) throws JSONException
	{
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject jsonObject=(JSONObject) jsonArray.get(i);
			HashMap<String, String> item=new HashMap<String, String>();
			item.put("Num","第"+(i+1)+"楼");
			item.put("ReplyUserName", jsonObject.getString("ReplyUserName"));
			item.put("ReplyUserID", jsonObject.getString("ReplyUserID"));
			item.put("SendTime", jsonObject.getString("CreateTime"));
			item.put("Content", jsonObject.getString("Content"));
			mCommentList.add(item);
		}
		mCommentAdapter.notifyDataSetChanged();
	}

	@Override
	public void loadMoreData() {
		if(mPageCount>=mIndex)
		{
			mIndex++;
			loadCommentData();
			if(mPageCount==mIndex)
			{
				mCommentListView.setLoading(false);
			};
		}
		
	}

	private void loadCommentData() {
		
		HashMap<String, String> params=new HashMap<String, String>();
		params.put("blogId", mBlogId);
		params.put("index", mIndex+"");
		params.put("sizePage", mPageSize+"");
		params.put("order", "true");
		new CallWebservice(this, "GetComment", params, GETCOMMENTLIST);
	}
}
