package org.shuaibi.hi_blog.activity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.shuaibi.hi_blog.R;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
/**
 * 
 * @author ZeroX
 *
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends Activity {
	@ViewById(R.id.iv_back)
	ImageView iv_back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	
	@Click(R.id.iv_back)
	public void back_Click()
	{
		finish();
	}
}
