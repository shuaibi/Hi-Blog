package org.shuaibi.hi_blog.view;

import org.shuaibi.hi_blog.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

/**
 * 
 * @描述: 带分页的列表
 * @作者: ZeorX
 * @时间: 2015-9-18
 * @参数： 
 * @接口： 
 *
 */
public class LoadListView extends ListView implements OnScrollListener {

	View mFooterView;

	int mTotalItemCount,mLastItem;
 	boolean isLoading=true;
 	
	public boolean isLoading() {
		return isLoading;
	}
	
	public void setLoading(boolean isLoading) {
		this.isLoading = isLoading;
	}

	ILoadInterface mILoadInterface;
	
	
	public LoadListView(Context context) {
		super(context);
		initView(context);
	}
	public LoadListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}
	public LoadListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView(context);
	}
	private void initView(Context context) {
		LayoutInflater layoutInflater=LayoutInflater.from(context);
		if(mFooterView==null){
			mFooterView=layoutInflater.inflate(R.layout.footer_layout,null);
		}
	    mFooterView.setVisibility(View.GONE);
		addFooterView(mFooterView);
		setOnScrollListener(this);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		this.mTotalItemCount=totalItemCount;
		this.mLastItem=firstVisibleItem+visibleItemCount;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if(scrollState==SCROLL_STATE_IDLE&&mLastItem==mTotalItemCount)
		{
			if(isLoading){
			 mFooterView.setVisibility(View.VISIBLE);
			 mILoadInterface.loadMoreData();
			}
		}

	}

	/**
	 * 设置接口
	 * @param mILoadInterface 加载数据接口
	 */
	public void setInterface(ILoadInterface mILoadInterface)
	{
		this.mILoadInterface=mILoadInterface;
		
	}
	

	
	
	/**
	 * 数据加载完毕
	 */
	public void loadCompany()
	{
		mFooterView.setVisibility(View.GONE);
	}
	
	/**
	 * 
	 * @描述: 加载数据接口
	 * @作者: ZeroX 
	 * @时间: 2015-9-18
	 * @参数： 
	 * @接口： 
	 *
	 */
	public interface ILoadInterface{
		void loadMoreData();
	}



}
