package org.shuaibi.hi_blog.adapter;

import java.util.HashMap;
import java.util.List;

import org.shuaibi.hi_blog.R;
import org.shuaibi.hi_blog.utils.CommonAdapter;
import org.shuaibi.hi_blog.utils.ViewHolder;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 *
 * @类名: CommentAdapter 
 * @说明: 评论列表适配器
 * @作者： ZeroX
 * @日期 2015-9-29
 *
 */
public class CommentAdapter extends CommonAdapter<HashMap<String, String>> {

	public CommentAdapter(Context context,List<HashMap<String, String>> list)
	{
		super.mContext=context;
		super.mList=list;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder=ViewHolder.get(mContext, convertView, parent, position, R.layout.item_comment);
		((TextView)viewHolder.getView(R.id.comment_tv_content)).setText(mList.get(position).get("Content"));
		((TextView)viewHolder.getView(R.id.comment_tv_number)).setText(mList.get(position).get("Num"));
		((TextView)viewHolder.getView(R.id.comment_tv_sendTime)).setText(mList.get(position).get("SendTime"));
		((TextView)viewHolder.getView(R.id.comment_tv_userName)).setText(mList.get(position).get("ReplyUserName"));
		return viewHolder.getConvertView();
	}

}
