package org.shuaibi.hi_blog.adapter;

import java.util.HashMap;
import java.util.List;

import org.shuaibi.hi_blog.R;
import org.shuaibi.hi_blog.utils.CommonAdapter;
import org.shuaibi.hi_blog.utils.ViewHolder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 
 * @描述: 博客列表适配器
 * @作者: ZeroX
 * @时间: 2015-9-17
 * @参数： 
 * @接口： 
 *
 */
public class BlogAdapter extends CommonAdapter<HashMap<String, String>> {

	
	public BlogAdapter(Context context,List<HashMap<String, String>> list)
	{
	   super.mContext=context;
	   super.mList=list;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder=ViewHolder.get(mContext, convertView, parent, position, R.layout.item_blog);
		((TextView)viewHolder.getView(R.id.tv_blog_userName)).setText(mList.get(position).get("UserName"));
		((TextView)viewHolder.getView(R.id.tv_blog_createTime)).setText(mList.get(position).get("CreateTime"));
		((TextView)viewHolder.getView(R.id.tv_blog_title)).setText(mList.get(position).get("Title"));
		((TextView)viewHolder.getView(R.id.tv_blog_content)).setText(mList.get(position).get("Content"));
		((TextView)viewHolder.getView(R.id.tv_blog_comment)).setText(mList.get(position).get("Comment").replace("null", "0")+"评论");
		((TextView)viewHolder.getView(R.id.tv_blog_visibility)).setText(mList.get(position).get("Visibility").replace("null", "0")+"阅读");
		return viewHolder.getConvertView();
	}

}
